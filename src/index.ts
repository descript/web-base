// 1. React
import React from "react"
export { React }

// 2. ReactDOM
import { render } from "react-dom"
export const attach = <P extends React.HTMLAttributes<T>, T extends Element>(
  app: React.DOMElement<P, T>
) => {
  const container = document.createElement("div")
  document.body.appendChild(container)
  render(app, container)
  return container
}

// 3. React Router
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
export { Router, Route, Switch }

// 4. Styled Components
import styled, {
  css,
  createGlobalStyle as globalStyle
} from "styled-components"
export { css, styled, globalStyle }
